var gameTable = document.getElementById('gameTable');
var gridDiv = document.getElementById('grid');
var newValueDiv = document.getElementById('newNumber');
var dots = document.getElementById('dots');
var scoreValue = document.getElementById('scoreValue');
var grid = [], newValuePosition, newValue, gameOver = false;
var showDebug = true;
var turn = 0, score = 0;
var totalTurns = 5;
for (var i = 0; i < 7; i++) {
    grid.push(new Array(7));
    var zeroValue = false, positiveValue = false;
    for (var ii = 0; ii < 7; ii++) {
        var div = createElement('box' + i + '' + ii, ['numCube', 'x' + i, 'y' + ii]);
        var radnomValue = getRandomNumber(0, 10) - 3;
        if (radnomValue == 0) {
            zeroValue = true;
        }
        else if (zeroValue) {
            radnomValue = 0;
        }
        else if (radnomValue > 0) {
            positiveValue = true;
        }
        else if (positiveValue && radnomValue < 0) {
            radnomValue = 0;
            zeroValue = true;
        }
        else if (radnomValue == -1) {
            radnomValue = -2;
        }
        grid[i][ii] = radnomValue;
        gameTable === null || gameTable === void 0 ? void 0 : gameTable.appendChild(div);
        var numDiv = createElement('value' + i + '' + ii, ['value', 'vx' + i, 'vy' + ii]);
        gridDiv.appendChild(numDiv);
    }
}
grid = [[7, 4, 6, 0, 0, 0, 0],
    [-2, -2, 5, 0, 0, 0, 0],
    [3, 2, 0, 0, 0, 0, 0],
    [-2, 1, 7, 5, 7, 2, 5],
    [7, 5, 0, 0, 0, 0, 0],
    [3, 4, 1, 0, 0, 0, 0],
    [1, 1, 2, 3, 3, 1, 0]];
updateTable();
var identifiedList = identifyAll(false);
while (identifiedList.length > 0) {
    identifiedList.forEach(function (xy) { return matchAction(xy[0], xy[1], false); });
    identifiedList.forEach(function (xy) { return fallColumn(xy[0]); });
    identifiedList = identifyAll(false);
}
updateTable();
createNewValue();
document.onkeydown = function (e) {
    if (gameOver) {
        return;
    }
    if (e.key == 'ArrowLeft' && newValuePosition > 0) {
        setNewValuePosition(newValuePosition - 1);
    }
    else if (e.key == 'ArrowRight' && newValuePosition < 6) {
        setNewValuePosition(newValuePosition + 1);
    }
    else if (e.key == 'ArrowDown' && newValuePosition != -1 && !grid[newValuePosition][6] && !gameOver) {
        turn++;
        var turnString = '';
        for (var i = 0; i < turn; i++) {
            turnString += '⚫ ';
        }
        for (var i = turn; i < totalTurns; i++) {
            turnString += '⚪ ';
        }
        dots.innerText = turnString;
        grid[newValuePosition][6] = newValue;
        updateTable();
        newValueDiv.innerText = '';
        var topValueY;
        for (topValueY = 5; topValueY >= 0; topValueY--) {
            if (grid[newValuePosition][topValueY]) {
                break;
            }
        }
        if (topValueY < 5) {
            grid[newValuePosition][6] = 0;
            grid[newValuePosition][topValueY + 1] = newValue;
            numberTransition(newValuePosition, 6, topValueY + 1).then(postDropAction);
        }
        else {
            updateTable();
            postDropAction();
        }
        // createNewValue();
        setTimeout(createNewValue, 250);
    }
};
function postDropAction() {
    var identifiedList = identifyAll();
    Promise.all(identifiedList.map(function (xy) { return matchAction(xy[0], xy[1]); })).then(function () {
        return Promise.all(identifiedList.map(function (xy) { return fallColumn(xy[0]); }));
    }).then(function () {
        identifiedList = identifyAll();
        if (identifiedList.length > 0) {
            postDropAction();
        }
        else if (turn == totalTurns) {
            for (var x = 0; x < 7; x++) {
                if (grid[x][6]) {
                    gameOver = true;
                    document.getElementById('turn').innerHTML = '<span id="gameover">GAME OVER</span>';
                    gridDiv.style.opacity = '.5';
                    gameTable.style.opacity = '.5';
                }
            }
            if (gameOver)
                return;
            turn = 0;
            var transitionPromises = [];
            for (var x = 0; x < 7; x++) {
                for (var y = 5; y >= 0; y--) {
                    transitionPromises.push(numberTransition(x, y, y + 1));
                    grid[x][y + 1] = grid[x][y];
                }
                grid[x][0] = -2;
            }
            Promise.all(transitionPromises).then(function () {
                updateTable();
                postDropAction();
            });
        }
    });
}
function createNewValue() {
    //test purpose only
    var prevValue = newValue;
    var debugString = 'New value: ' + prevValue + ' for turn ' + turn + '\n[';
    for (var i = 0; i < 7; i++) {
        debugString += '[' + grid[i].join(',') + '],';
    }
    debugString += ']';
    trace(debugString);
    //------------------------
    newValue = getRandomNumber(0, 7);
    newValueDiv.innerText = newValue.toString();
    setNewValuePosition(3);
}
function getRandomNumber(min, max) {
    var diff = max - min;
    return Math.ceil(min + Math.random() * diff);
}
function createElement(id, classes) {
    var div = document.createElement('div');
    div.id = id;
    classes.forEach(function (className) { return div.classList.add(className); });
    return div;
}
function updateTable() {
    for (var i = 0; i < 7; i++) {
        for (var ii = 0; ii < 7; ii++) {
            var currentDiv = getNumGrid(i, ii);
            var currentValue = grid[i][ii];
            if (currentValue < 0) {
                var block = createElement('block' + i + '' + ii, ['block', 'block' + currentValue]);
                currentDiv.innerHTML = '';
                currentDiv.appendChild(block);
            }
            else if (currentValue > 0) {
                currentDiv.innerText = grid[i][ii] + '';
            }
            else {
                currentDiv.innerHTML = '';
            }
        }
    }
}
function trace(value) {
    if (showDebug)
        console.log(value);
}
function identifyAll(updateScore) {
    if (updateScore === void 0) { updateScore = true; }
    var identiFiedList = [];
    for (var x = 0; x < 7; x++) {
        for (var y = 0; y < 7; y++) {
            if (identify(x, y)) {
                identiFiedList.push([x, y]);
                if (updateScore) {
                    score++;
                }
            }
        }
    }
    return identiFiedList;
}
function identify(x, y) {
    var currentValue = grid[x][y];
    if (currentValue == 0 || currentValue == -1 || currentValue == -2) {
        return false;
    }
    var match = false;
    var totalCount = 1;
    for (var i = x + 1; i < 7; i++) {
        if (!grid[i][y])
            break;
        totalCount++;
    }
    for (var i = x - 1; i >= 0; i--) {
        if (!grid[i][y])
            break;
        totalCount++;
    }
    if (totalCount == currentValue) {
        document.getElementById('box' + x + y).style.backgroundColor = 'red';
        match = true;
    }
    totalCount = 1;
    for (var i = y + 1; i < 7; i++) {
        if (!grid[x][i])
            break;
        totalCount++;
    }
    for (var i = y - 1; i >= 0; i--) {
        if (!grid[x][i])
            break;
        totalCount++;
    }
    if (totalCount == currentValue) {
        document.getElementById('box' + x + y).style.backgroundColor = 'red';
        match = true;
    }
    return match;
}
function matchAction(x, y, postScaleAction) {
    if (postScaleAction === void 0) { postScaleAction = true; }
    scoreValue.innerText = '' + score;
    grid[x][y] = 0;
    var yellowBoxes = [];
    getNumGrid(x, y).style.transform = 'scale(0)';
    if (x > 0) {
        crack(x - 1, y);
    }
    if (x < 6) {
        crack(x + 1, y);
    }
    if (y > 0) {
        crack(x, y - 1);
    }
    if (y < 6) {
        crack(x, y + 1);
    }
    return new Promise(function (resolve, reject) { return setTimeout(function () {
        getBGGrid(x, y).style.backgroundColor = 'transparent';
        yellowBoxes.forEach(function (div) { return div.style.backgroundColor = 'transparent'; });
        if (postScaleAction) {
            getNumGrid(x, y).innerHTML = '';
        }
        resolve();
    }, 500); });
    function crack(x, y) {
        if (grid[x][y] == -2) {
            grid[x][y] = -1;
            document.getElementById('block' + x + '' + y).style.background = 'gray';
        }
        else if (grid[x][y] == -1) {
            document.getElementById('block' + x + '' + y).style.transform = 'scale(0)';
            var number_1 = getRandomNumber(0, 7);
            grid[x][y] = number_1;
            setTimeout(function () { return getNumGrid(x, y).innerHTML = number_1.toString(); }, 300);
        }
        else {
            return;
        }
        var yellowGrid = getBGGrid(x, y);
        yellowGrid.style.backgroundColor = 'yellow';
        yellowBoxes.push(yellowGrid);
    }
}
function getNumGrid(x, y) {
    return document.getElementById('value' + x + y);
}
function getBGGrid(x, y) {
    return document.getElementById('box' + x + y);
}
function fallColumn(x) {
    var fallStart = -1, fallEnd = 0;
    for (var i = 0; i < 7; i++) {
        if (grid[x][i] == 0) {
            if (i == 6) {
                break;
            }
            if (fallStart == -1) {
                fallStart = i;
            }
        }
        else if (fallStart != -1) {
            fallEnd = i;
            break;
        }
    }
    if (fallEnd) {
        var transitionList = [];
        var diff = fallEnd - fallStart;
        for (var i = fallStart; i <= 6 - diff; i++) {
            var a = grid[x][i + diff];
            grid[x][i] = a;
            transitionList.push([i + diff, i]);
        }
        for (var i = 7 - diff; i < 7; i++) {
            grid[x][i] = 0;
        }
        fallColumn(x);
        return transitionList.map(function (trPair) { return numberTransition(x, trPair[0], trPair[1]); });
    }
    else {
        // updateTable();
    }
}
function setNewValuePosition(position) {
    newValuePosition = position;
    newValueDiv.style.left = (2.3 + 7 * position) + 'rem';
    for (var i = 0; i < 7; i++) {
        getNumGrid(position, i).style.transform = 'scale(1)';
    }
}
function numberTransition(xPosition, yStart, yEnd) {
    var destDivName = 'value' + xPosition + '' + yEnd;
    gridDiv.removeChild(document.getElementById(destDivName));
    var div = document.getElementById('value' + xPosition + '' + yStart);
    div.classList.add('vy' + yEnd);
    div.classList.remove('vy' + yStart);
    div.id = destDivName;
    gridDiv.appendChild(createElement('value' + xPosition + '' + yStart, ['vx' + xPosition, 'vy' + yStart, 'value']));
    return new Promise(function (resolve, reject) { return setTimeout(resolve, 250); });
}
