const gameTable = document.getElementById('gameTable') as HTMLDivElement;
const gridDiv = document.getElementById('grid');
const newValueDiv = document.getElementById('newNumber');
const dots = document.getElementById('dots');
const scoreValue = document.getElementById('scoreValue');
let grid: number[][] = [], newValuePosition: number, newValue: number, gameOver = false;
const showDebug = true;
let turn = 0, score = 0;
const totalTurns = 5;

for(let i = 0; i < 7; i++) {
    grid.push(new Array(7));
    let zeroValue = false, positiveValue = false;
    for(let ii = 0; ii < 7; ii++) {
        const div = createElement('box' + i + '' + ii, ['numCube', 'x' + i, 'y' + ii])
        let radnomValue = getRandomNumber(0, 10) - 3;

        if(radnomValue == 0) { 
            zeroValue = true;
        } else if(zeroValue) {
            radnomValue = 0;
        }else if(radnomValue > 0) {
            positiveValue = true;
        } else if(positiveValue && radnomValue < 0) {
            radnomValue = 0;
            zeroValue = true;
        } else if(radnomValue == -1) {
            radnomValue = -2;
        }

        grid[i][ii] = radnomValue;
        gameTable?.appendChild(div);

        const numDiv = createElement('value' + i + '' + ii, ['value', 'vx' + i, 'vy' + ii])
        gridDiv.appendChild(numDiv);
    }
}

grid = [[7,4,6,0,0,0,0],
    [-2,-2,5,0,0,0,0],
    [3,2,0,0,0,0,0],
    [-2,1,7,5,7,2,5],
    [7,5,0,0,0,0,0],
    [3,4,1,0,0,0,0],
    [1,1,2,3,3,1,0]]

updateTable();

let identifiedList = identifyAll(false);
while(identifiedList.length > 0) {
    identifiedList.forEach(xy => matchAction(xy[0], xy[1], false));
    identifiedList.forEach(xy => fallColumn(xy[0]));
    identifiedList = identifyAll(false);
}
updateTable();
createNewValue();


document.onkeydown = (e: KeyboardEvent) => {
    if(gameOver) {
        return;
    }

    if(e.key == 'ArrowLeft' && newValuePosition > 0) {
        setNewValuePosition(newValuePosition - 1);
    } else if(e.key == 'ArrowRight' && newValuePosition < 6) {
        setNewValuePosition(newValuePosition + 1);
    } else if(e.key == 'ArrowDown' && newValuePosition != -1 && !grid[newValuePosition][6] && !gameOver) {
        
        turn++;
        let turnString = '';
        for(let i = 0; i < turn; i++) {
            turnString += '⚫ ';
        }
        for(let i = turn; i < totalTurns; i++) {
            turnString += '⚪ ';
        }
        dots.innerText = turnString;

        grid[newValuePosition][6] = newValue;
        
        updateTable();
        newValueDiv.innerText = '';
        var topValueY: number;

        for(topValueY = 5; topValueY >= 0; topValueY--) {
            if(grid[newValuePosition][topValueY]) {
                break;
            }
        }

        if(topValueY < 5) {
            grid[newValuePosition][6] = 0;
            grid[newValuePosition][topValueY + 1] = newValue;
            numberTransition(newValuePosition, 6, topValueY + 1).then(postDropAction);
        } else {
            updateTable();
            postDropAction();
        }

        // createNewValue();
        setTimeout(createNewValue, 250);
    }
}

function postDropAction() {
    let identifiedList = identifyAll(); 
    Promise.all(identifiedList.map(xy => matchAction(xy[0], xy[1]))).then(() => {
        return Promise.all(identifiedList.map(xy => fallColumn(xy[0])))
    }).then(() => {
        identifiedList = identifyAll();
        if(identifiedList.length > 0) {
            postDropAction();
        } else if(turn == totalTurns) {
            for(let x = 0; x < 7; x++) {
                if(grid[x][6]) {
                    gameOver = true;
                    document.getElementById('turn').innerHTML = '<span id="gameover">GAME OVER</span>'
                    gridDiv.style.opacity = '.5';
                    gameTable.style.opacity = '.5';

                }
            }
            if(gameOver) return;
            
            turn = 0;
            const transitionPromises: Promise<void>[] = [];
            for(let x = 0; x < 7; x++) {
                for (let y = 5; y >= 0; y--) {
                    transitionPromises.push(numberTransition(x, y, y + 1));
                    grid[x][y + 1] = grid[x][y];
                }
                grid[x][0] = -2;
            }
            Promise.all(transitionPromises).then(() => {
                updateTable();
                postDropAction();
            });
        }
    })
}

function createNewValue() { //Creating a new value on top after every turn
    //test purpose only
    const prevValue = newValue
    let debugString = 'New value: ' + prevValue + ' for turn ' + turn + '\n[';
    for(let i = 0; i < 7; i++) {
        debugString += '[' + grid[i].join(',') + '],' 
    }
    debugString += ']'
    trace(debugString);
    //------------------------
    
    newValue = getRandomNumber(0, 7);
    newValueDiv.innerText = newValue.toString();
    setNewValuePosition(3);
}

function getRandomNumber(min: number, max: number): number { //Random number between min and max
    const diff = max - min;
    return Math.ceil(min + Math.random() * diff);
}

function createElement(id: string, classes: string[]): HTMLDivElement {//Create new div element with id and classes
    const div = document.createElement('div');
    div.id = id;
    classes.forEach((className) => div.classList.add(className));

    return div;
}

function updateTable() {// Update table values in grid div accoriding to div without any animation
    for(let i = 0; i < 7; i++) {
        for(let ii = 0; ii < 7; ii++) {

            const currentDiv = getNumGrid(i, ii);
            const currentValue = grid[i][ii];

            if(currentValue < 0) {
                const block = createElement('block' + i + '' + ii, ['block', 'block' + currentValue]);
                currentDiv.innerHTML = '';
                currentDiv.appendChild(block);
            }
            else if(currentValue > 0) {
                currentDiv.innerText = grid[i][ii] + '';
            } else {
                currentDiv.innerHTML = '';
            }
        }
    }
}

function trace(value: any) {
    if(showDebug) console.log(value);
}


function identifyAll(updateScore: boolean = true): number[][] {//identyfy all numbers
    
    const identiFiedList: number[][] = [];
    for(let x = 0; x < 7; x++) {
        for(let y = 0; y < 7; y++) {
            if(identify(x, y)) {
                identiFiedList.push([x, y]);
                if(updateScore){
                    score++;
                } 
            }
        }
    }
    
    return identiFiedList;
    
}

function identify(x: number, y: number): boolean {//identify if a number is eligible for breaking
    const currentValue = grid[x][y];
    if(currentValue == 0 || currentValue == -1 || currentValue == -2) {
        return false;
    }

    let match = false;
    
    let totalCount = 1;
    for(let i = x + 1; i < 7; i++) {
        if(!grid[i][y]) break;
        totalCount++;
    }
    for(let i = x - 1; i >= 0; i--) {
        if(!grid[i][y]) break;
        totalCount++;
        
    }
    if(totalCount == currentValue) {
        document.getElementById('box' + x + y).style.backgroundColor = 'red';
        match = true;
    }

    totalCount = 1;
    for(let i = y + 1; i < 7; i++) {
        if(!grid[x][i]) break;
        totalCount++;
        
    }
    for(let i = y - 1; i >= 0; i--) {
        if(!grid[x][i]) break;
        totalCount++;
        
    }
    if(totalCount == currentValue) {
        document.getElementById('box' + x + y).style.backgroundColor = 'red';
        match = true;
    }

    return match;
}

function matchAction(x: number, y: number, postScaleAction = true): Promise<void> {// take action for 0 valued cell
    scoreValue.innerText = '' + score;
    
    grid[x][y] = 0;
    const yellowBoxes: HTMLDivElement[] = [];
    getNumGrid(x, y).style.transform = 'scale(0)';
    
    if(x > 0) {
        crack(x - 1, y);
    } 
    if(x < 6) {
        crack(x + 1, y);
    } 
    if(y > 0) {
        crack(x, y - 1);
    } 
    if(y < 6) {
        crack(x, y + 1)
    }

    return new Promise((resolve, reject) => setTimeout(() => {
        getBGGrid(x, y).style.backgroundColor = 'transparent';
        yellowBoxes.forEach(div => div.style.backgroundColor = 'transparent');
        if(postScaleAction){
            getNumGrid(x, y).innerHTML = '';
        }
        
        resolve();
    }, 500))

    function crack(x: number, y: number){
        if(grid[x][y] == -2) {
            grid[x][y] = -1;

            document.getElementById('block' + x + '' + y).style.background = 'gray';
        } else if(grid[x][y] == -1) {
            document.getElementById('block' + x + '' + y).style.transform = 'scale(0)';
            const number = getRandomNumber(0, 7)
            grid[x][y] = number;
            setTimeout(() => getNumGrid(x, y).innerHTML = number.toString(), 300)
            
        } else {
            return;
        }

        const yellowGrid = getBGGrid(x, y);
        yellowGrid.style.backgroundColor = 'yellow';
        yellowBoxes.push(yellowGrid);
    }
}

function getNumGrid(x: number, y: number): HTMLDivElement {//get a div in grid based on x y
    return document.getElementById('value' + x + y) as HTMLDivElement;
}

function getBGGrid(x: number, y: number): HTMLDivElement {//get a div in gametable based on x y
    return document.getElementById('box' + x + y) as HTMLDivElement;
}

function fallColumn(x: number): Promise<void>[] {// bring down numbers in grid array only to eliminate 0 values
    let fallStart = -1, fallEnd = 0;
    
    for(let i = 0; i < 7; i++) {
        if(grid[x][i] == 0) {
            if(i == 6) {
                break;
            }
            if(fallStart == -1) {
                fallStart = i;
            }
        } else if(fallStart != -1) {
            fallEnd = i;
            break;
        }
    }

    if(fallEnd) {
        const transitionList: number[][] = [];
        const diff = fallEnd - fallStart;
        for(let i = fallStart; i <= 6 - diff; i++) {
            const a = grid[x][i + diff];
            grid[x][i] = a;
            transitionList.push([i + diff, i])
        }

        for(let i = 7 - diff; i < 7; i++) {
            grid[x][i] = 0;
        }


        fallColumn(x);
        return transitionList.map(trPair => numberTransition(x, trPair[0], trPair[1]));
    } else {
        // updateTable();
    }
}

function setNewValuePosition(position: number) {
    newValuePosition = position;
    newValueDiv.style.left = (2.3 + 7 * position) + 'rem';

    for(let i = 0; i < 7; i++) {
        getNumGrid(position, i).style.transform = 'scale(1)';
    }
}

function numberTransition(xPosition: number, yStart: number, yEnd: number): Promise<null> {// Animation for falling numbers
    const destDivName = 'value' + xPosition + '' + yEnd;
    gridDiv.removeChild(document.getElementById(destDivName));

    const div = document.getElementById('value' + xPosition + '' + yStart);
    div.classList.add('vy' + yEnd);
    div.classList.remove('vy' + yStart);
    div.id = destDivName;

    gridDiv.appendChild(createElement('value' + xPosition + '' + yStart, ['vx' + xPosition, 'vy' + yStart, 'value']));

    return new Promise((resolve, reject) => setTimeout(resolve, 250))

}
